package com.challenge.plane

import com.challenge.plane.model.Group
import com.challenge.plane.model.Passenger
import com.challenge.plane.model.Plane
import com.challenge.plane.model.Seat

class Allocator {

    Plane plane

    Allocator(){

    }

    List<Group> groups = []

    void addGroup(Group g ){

        groups.add(g)

        plane.init()

        groups.each{Group group->
            group.passengers.each{
                it.seat=null
            }
        }

        groups.sort{
            -1*it.passengers.size()
        }

        groups.each{ Group group ->
            allocateGroup(group)
        }
    }

    protected void allocateGroup(Group group){
        List<Seat[]> availables = plane.groupFreeSeatsWithWindowBy(group.passengers.size(), group.numWindows)

        if( availables.size() == 0)
            availables = plane.groupFreeSeatsBy(group.passengers.size())

        if( availables.size() == 0)
            return

        availables.sort{-1*it.length}

        List<Seat> seats = availables.first() as List<Seat>
        List<Passenger> passengers = group.passengers

        passengers.findAll{it.window}.each{ Passenger p ->
            Seat seat= seats.find { it.window } ?: seats.first()
            seat.allocate(p)
            seats.remove(seat)
        }

        passengers.findAll{!it.window}.each{ Passenger p ->
            Seat seat = seats.first()
            seat.allocate(p)
            seats.remove(seat)
        }
    }

    float getSatisfaction(){
        int points = 0
        int count=0

        groups.each{ Group g ->
            g.passengers.each{ Passenger p->
                count++
                if( p.seat ){
                    if( p.window )
                        points += p.seat.window ? 1 : 0
                    else
                        points++
                }
            }
        }

        points / count
    }


}
