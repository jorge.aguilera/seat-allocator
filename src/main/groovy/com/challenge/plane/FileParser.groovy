package com.challenge.plane

import com.challenge.plane.model.Plane
import com.challenge.plane.model.Group
import com.challenge.plane.model.Passenger
import groovy.transform.CompileDynamic

import java.util.regex.Matcher

class FileParser {

    Allocator allocator

    @CompileDynamic
    void initialize(String line){
        Matcher group = (line  =~ /([0-9]+) ([0-9]+)/)
        assert group.hasGroup()
        assert group.size()==1
        assert group[0].size()==3
        allocator.plane = new Plane(group[0][1] as int,group[0][2] as int)
        return
    }

    @CompileDynamic
    void parseGroup(String line){
        Matcher group = (line  =~ /([0-9]+W?)+/)
        assert group.hasGroup()

        Group g = new Group()
        group.each{ item->
            String str = "${item[1]}"
            boolean window = false
            if(str.endsWith('W')){
                window=true
                str = str[0..-2]
            }
            g.passengers.add(new Passenger(id:str as int, window:window))
        }
        allocator.addGroup(g)
    }


    void parseInputStream(InputStream inputStream){
        assert allocator!=null

        inputStream.readLines().eachWithIndex{ String line, int i ->
            if( i == 0 ){
                initialize(line)
                return
            }

            parseGroup(line)
        }
    }

}
