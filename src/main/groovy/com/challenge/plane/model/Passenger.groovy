package com.challenge.plane.model

class Passenger {

    int id

    boolean window

    Seat seat

    String toString() {
        String.format(" %02d ",id)
    }
}
