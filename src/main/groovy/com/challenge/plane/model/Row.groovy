package com.challenge.plane.model

class Row {

    int id

    Seat[] seats


    List<Seat[]> groupFreeSeatsBy(int n){

        List<Seat[]> ret = []
        for(int i=0; i<seats.length; i++){
            if( seats[i].passenger )
                continue
            int j=i
            for(; j<seats.length; j++){
                if( seats[j].passenger ){
                    break
                }
            }
            if( j - i >= n)
                ret.add seats[i..i+n-1] as Seat[]
        }

        ret
    }

    String toString(){
        StringBuffer sb = new StringBuffer()
        seats.each{ Seat s->
            sb.append "${s.passenger ? s.passenger : ' _ '}"
        }
        sb.toString()
    }
}
