package com.challenge.plane.model

import groovy.transform.ToString

@ToString
class Seat {

    int id

    boolean window

    Passenger passenger

    void allocate(Passenger passenger){
        this.passenger=passenger
        passenger.seat=this
    }
}
