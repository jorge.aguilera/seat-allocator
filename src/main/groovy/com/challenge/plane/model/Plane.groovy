package com.challenge.plane.model

class Plane {

    List<Row> rows = []

    int maxRows

    int maxSeats

    private int capacity
    int getCapacity(){
        capacity
    }

    Plane(int maxRows, int maxSeats){
        this.maxRows=maxRows
        this.maxSeats=maxSeats
        init()
    }

     void init( ){
        assert maxRows > 0
        assert maxSeats > 0

        capacity=0
        rows = []
        (1..maxRows).each{ iw ->
            List<Seat> seats = []
            (1..maxSeats).each{ ih->
                Seat s = new Seat(id: capacity++, window: ih==1 || ih==maxSeats )
                seats.add s
            }
            Row row = new Row(id: iw, seats: seats as Seat[])
            rows.add row
        }
    }

    Passenger[] getPassengers(){
        List<Passenger>ret=[]
        rows.each{
            ret.addAll it.seats.findAll{ Seat seat-> seat.passenger }*.passenger
        }
        ret as Passenger[]
    }

    List<Seat[]> groupFreeSeatsWithWindowBy(int groupSize, int window){
        List<Seat[]> ret = []
        rows.each{

            List<Seat[]> frees = it.groupFreeSeatsBy(groupSize).findAll{
                if( window == 1)
                    return it.first().window || it.last().window
                if( window == 2)
                    return it.first().window && it.last().window
                true
            }

            ret.addAll frees
        }
        ret
    }


    List<Seat[]> groupFreeSeatsBy(int groupSize){
        List<Seat[]> ret = []
        rows.each{
            ret.addAll it.groupFreeSeatsBy(groupSize)
        }
        ret
    }

    String toString(){
        StringBuffer sb = new StringBuffer()
        rows.each{ Row row->
            sb.append "$row\n"
        }
        sb.toString()
    }
}
