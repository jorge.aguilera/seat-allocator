package com.challenge.plane.model
import groovy.transform.ToString

@ToString
class Group {

    List<Passenger> passengers = []

    int getNumWindows(){
        passengers.findAll{ it.window }.size()
    }

}
