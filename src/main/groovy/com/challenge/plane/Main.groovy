package com.challenge.plane

class Main {

    static void main(String[]args){

        if(args.length != 1){
            println "Must to provide a filename"
            System.exit(-1)
            return
        }

        File f = new File(args[0])
        if( !f.exists() ){
            println "Must to provide an existing file path"
            System.exit(-1)
            return
        }

        Allocator allocator = new Allocator()
        FileParser fileParser = new FileParser(allocator:allocator)
        fileParser.parseInputStream(f.newInputStream())

        println allocator.plane
        println "${(100*allocator.satisfaction).round()}%"
    }

}
