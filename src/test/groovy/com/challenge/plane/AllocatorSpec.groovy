package com.challenge.plane

import com.challenge.plane.model.Plane
import com.challenge.plane.model.Group
import com.challenge.plane.model.Passenger
import spock.lang.Specification

class AllocatorSpec extends Specification{

    def "allocate no more than #n passenger is #weCan"() {

        when:
        Plane plane = new Plane(maxRows, maxSeats)

        and:
        Allocator allocator = new Allocator(plane:plane)

        and:
        int i=0
        List<Passenger> passengers = (0..n-1).collect {
            Passenger passenger = new Passenger(id: i++, window: false)
        }
        Group group = new Group(passengers: passengers)

        and:
        allocator.addGroup(group)
        println plane

        then:
        (plane.passengers.size() != 0) == weCan

        where:
        maxRows  |  maxSeats    | n | weCan
        4        |  3           | 1 | true
        4        |  3           | 2 | true
        4        |  3           | 3 | true
        4        |  3           | 4 | false
    }

    def "allocate more than #g groups"() {

        given:
        Plane plane = new Plane(4,4)

        and:
        Allocator allocator = new Allocator(plane:plane)

        when:
        int i=0
        List<Passenger> passengers1 = (0..1).collect {
            Passenger passenger = new Passenger(id: i++, window: false)
        }
        Group group1 = new Group(passengers: passengers1)

        allocator.addGroup(group1)

        and:
        List<Passenger> passengers2 = (0..2).collect {
            Passenger passenger = new Passenger(id: i++, window: false)
        }
        Group group2 = new Group(passengers: passengers2)

        allocator.addGroup(group2)

        and:
        List<Passenger> passengers3 = (0..1).collect {
            Passenger passenger = new Passenger(id: i++, window: false)
        }
        Group group3 = new Group(passengers: passengers3)

        allocator.addGroup(group3)

        and:
        List<Passenger> passengers4 = (0..0).collect {
            Passenger passenger = new Passenger(id: i++, window: false)
        }
        Group group4 = new Group(passengers: passengers4)

        allocator.addGroup(group4)

        println plane

        then:
        plane.passengers.size() != 0
    }

    def "can allocate no more than #n passenger with window ? #weCan"() {

        when:
        Plane plane = new Plane(maxRows, maxSeats)

        and:
        Allocator allocator = new Allocator(plane:plane)

        and:
        int i=0
        List<Passenger> passengers = (0..n-1).collect {
            Passenger passenger = new Passenger(id: i++, window: i%2)
        }
        Group group = new Group(passengers: passengers)

        and:
        allocator.addGroup(group)
        println plane

        then:
        (plane.passengers.size() != 0) == weCan

        where:
        maxRows  |  maxSeats    | n | weCan
        4        |  3           | 1 | true
        4        |  3           | 2 | true
        4        |  3           | 3 | true
        4        |  3           | 4 | false
    }

    def "given a 4x4 plane we can allocate groups with 100%"() {

        given:
        Plane plane = new Plane(4,4)

        and:
        Allocator allocator = new Allocator(plane:plane)

        when:
        int i=1
        Passenger p1 = new Passenger(id:i++,window: true)
        Passenger p2 = new Passenger(id:i++,window: false)
        Passenger p3 = new Passenger(id:i++,window: false)
        Passenger p4 = new Passenger(id:i++,window: false)
        Passenger p5 = new Passenger(id:i++,window: false)
        Passenger p6 = new Passenger(id:i++,window: false)
        Passenger p7 = new Passenger(id:i++,window: false)
        Passenger p8 = new Passenger(id:i++,window: false)
        Passenger p9 = new Passenger(id:i++,window: false)
        Passenger p10 = new Passenger(id:i++,window: false)
        Passenger p11 = new Passenger(id:i++,window: true)
        Passenger p12 = new Passenger(id:i++,window: true)
        Passenger p13 = new Passenger(id:i++,window: false)
        Passenger p14 = new Passenger(id:i++,window: false)
        Passenger p15 = new Passenger(id:i++,window: false)
        Passenger p16 = new Passenger(id:i++,window: false)

        Group g1 = new Group(passengers: [p1,p2,p3])
        Group g2 = new Group(passengers: [p4,p5,p6,p7])
        Group g3 = new Group(passengers: [p8])
        Group g4 = new Group(passengers: [p9,p10,p11])
        Group g5 = new Group(passengers: [p12])
        Group g6 = new Group(passengers: [p13,p14])
        Group g7 = new Group(passengers: [p15,p16])

        and:
        allocator.addGroup(g1)
        allocator.addGroup(g2)
        allocator.addGroup(g3)
        allocator.addGroup(g4)
        allocator.addGroup(g5)
        allocator.addGroup(g6)
        allocator.addGroup(g7)

        println plane
        println "Satisfaction ${(allocator.satisfaction*100).round()}%\n"

        then:
        (100*allocator.satisfaction).round() == 100
    }

    def "given a #n x #m plane when all want window we can allocate groups with #satisfaction %"() {

        when:
        Plane plane = new Plane(n,m)

        and:
        Allocator allocator = new Allocator(plane:plane)

        and:
        int i=1
        Passenger p1 = new Passenger(id:i++,window: true)
        Passenger p2 = new Passenger(id:i++,window: true)
        Passenger p3 = new Passenger(id:i++,window: true)
        Passenger p4 = new Passenger(id:i++,window: true)
        Passenger p5 = new Passenger(id:i++,window: true)
        Passenger p6 = new Passenger(id:i++,window: true)
        Passenger p7 = new Passenger(id:i++,window: true)
        Passenger p8 = new Passenger(id:i++,window: true)
        Passenger p9 = new Passenger(id:i++,window: true)
        Passenger p10 = new Passenger(id:i++,window: true)
        Passenger p11 = new Passenger(id:i++,window: true)
        Passenger p12 = new Passenger(id:i++,window: true)
        Passenger p13 = new Passenger(id:i++,window: true)
        Passenger p14 = new Passenger(id:i++,window: true)
        Passenger p15 = new Passenger(id:i++,window: true)
        Passenger p16 = new Passenger(id:i++,window: true)

        Group g1 = new Group(passengers: [p1,p2,p3])
        Group g2 = new Group(passengers: [p4,p5,p6,p7])
        Group g3 = new Group(passengers: [p8])
        Group g4 = new Group(passengers: [p9,p10,p11])
        Group g5 = new Group(passengers: [p12])
        Group g6 = new Group(passengers: [p13,p14])
        Group g7 = new Group(passengers: [p15,p16])

        and:
        allocator.addGroup(g1)
        allocator.addGroup(g2)
        allocator.addGroup(g3)
        allocator.addGroup(g4)
        allocator.addGroup(g5)
        allocator.addGroup(g6)
        allocator.addGroup(g7)

        println plane
        println "Satisfaction ${(allocator.satisfaction*100).round()}%\n"

        then:
        allocator.satisfaction == satisfaction

        where:
        n | m | satisfaction
        4 | 4 | 8/16
        3 | 3 | 6/16
        10 | 1 | 2/16
    }

}

