package com.challenge.plane

import com.boxever.plane.model.*
import com.challenge.plane.model.Passenger
import com.challenge.plane.model.Row
import com.challenge.plane.model.Seat
import spock.lang.Specification

class GroupSpec  extends Specification{

    def "given a row with #seats we can split in availables #n groups with #c capacity"() {

        when:
        Row row = new Row(seats: seats)

        and:
        List<Seat[]> result = row.groupFreeSeatsBy(capacity)

        then:
        result.size() == groups
        result.find{ it.length < capacity } == null

        where:
        seats               | capacity | groups
        [new Seat(id:0)]    | 1         | 1

        [new Seat(id:0),
         new Seat(id:1)]    | 1         | 2

        [new Seat(id:0),
         new Seat(id:1)]    | 2         | 1

        [new Seat(id:0),
         new Seat(id:1),
         new Seat(id:2)]    | 1         | 3

        [new Seat(id:0),
         new Seat(id:1),
         new Seat(id:2)]    | 2         | 2

        [new Seat(id:0),
         new Seat(id:1),
         new Seat(id:2),
         new Seat(id:3)]    | 1         | 4

        [new Seat(id:0),
         new Seat(id:1),
         new Seat(id:2),
         new Seat(id:3)]    | 2         | 3
    }

    def "given a row with #seats busies we can split in availables #n groups with #c capacity"() {

        when:
        Row row = new Row(seats: seats)

        and:
        List<Seat[]> result = row.groupFreeSeatsBy(capacity)

        then:
        result.size() == groups
        result.find{ it.length < capacity } == null

        where:
        seats                                        | capacity | groups
        [new Seat(id:0, passenger: new Passenger())] | 1        | 0

        [new Seat(id:0),
         new Seat(id:1, passenger: new Passenger())] | 1        | 1

        [new Seat(id:0),
         new Seat(id:1, passenger: new Passenger()),
         new Seat(id:2)]                             | 1        | 2

        [new Seat(id:0),
         new Seat(id:1),
         new Seat(id:2, passenger: new Passenger())] | 1        | 2

        [new Seat(id:0),
         new Seat(id:1),
         new Seat(id:2, passenger: new Passenger()),
         new Seat(id:3),]                            | 1        | 3

        [new Seat(id:0),
         new Seat(id:1),
         new Seat(id:2, passenger: new Passenger()),
         new Seat(id:3),]                               | 2         | 1

        [new Seat(id:0),
         new Seat(id:1),
         new Seat(id:2),
         new Seat(id:3, passenger: new Passenger()),]   | 2         | 2

        [new Seat(id:0),
         new Seat(id:1, passenger: new Passenger()),
         new Seat(id:2),
         new Seat(id:3, passenger: new Passenger()),]   | 2         | 0

        [new Seat(id:0),
         new Seat(id:1, passenger: new Passenger()),
         new Seat(id:2),
         new Seat(id:3, passenger: new Passenger()),
         new Seat(id:4, passenger: new Passenger())]   | 2         | 0

        [new Seat(id:0),
         new Seat(id:1, passenger: new Passenger()),
         new Seat(id:2),
         new Seat(id:3, passenger: new Passenger()),
         new Seat(id:4, passenger: new Passenger())]   | 1         | 2
    }

}
