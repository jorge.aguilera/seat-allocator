package com.challenge.plane

import spock.lang.Specification

class FileSpec extends Specification{

    def "given a #filename parse first line"() {

        when:
        FileParser fileParser = new FileParser(allocator: new Allocator())

        and:
        InputStream is = this.getClass().getResourceAsStream(filename)

        and:
        fileParser.initialize(is.readLines().first())

        then:
        fileParser.allocator.plane.maxRows == m
        fileParser.allocator.plane.maxSeats == n

        where:
        filename | m | n
        "/1.txt"  | 4 | 4
        "/2.txt"  | 3 | 3
    }

    def "given a #filename parse first group line"() {

        when:
        FileParser fileParser = new FileParser(allocator: new Allocator())

        and:
        InputStream is = this.getClass().getResourceAsStream(filename)

        and:
        List<String> lines = is.readLines()
        fileParser.initialize(lines.first())

        and:
        fileParser.parseGroup(lines.drop(1).first())

        then:
        fileParser.allocator.plane.maxRows == m
        fileParser.allocator.plane.maxSeats == n

        and:
        fileParser.allocator.plane.passengers.find{it.seat==null} == null

        and:
        fileParser.allocator.plane.passengers.find{it.id==passenger}.window == window

        where:
        filename | m | n  | passenger | window
        "/1.txt"  | 4 | 4 | 1       | true
        "/2.txt"  | 3 | 3 | 23       | false
    }

    def "given a #filename parse the lines"() {

        when:
        FileParser fileParser = new FileParser(allocator: new Allocator())

        and:
        InputStream is = this.getClass().getResourceAsStream(filename)

        and:
        List<String> lines = is.readLines()
        fileParser.initialize(lines.first())

        and:
        for(int i=1;i<lines.size();i++)
            fileParser.parseGroup(lines[i])

        then:
        fileParser.allocator.plane.maxRows == m
        fileParser.allocator.plane.maxSeats == n

        and:
        fileParser.allocator.satisfaction == satisfaction

        where:
        filename | m | n  | satisfaction
        "/1.txt"  | 4 | 4 | 16/16
        "/2.txt"  | 3 | 3 | 9/16
    }

}
