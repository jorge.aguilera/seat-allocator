package com.challenge.plane

import com.challenge.plane.model.Plane
import spock.lang.Specification

class PlaneSpec extends Specification{

    def "Given #maxRows and #maxSeats we have an initialized plane"(){

        when:
        Plane plane = new Plane(maxRows, maxSeats)

        then:
        plane.rows.size()==maxRows

        and:
        plane.rows[0].seats.size()==maxSeats

        and:
        plane.rows[0].seats.size() == maxSeats
        plane.rows[0].seats[0].id == 0
        plane.rows[0].seats[0].window

        plane.rows[0].seats[maxSeats-1].id == maxSeats-1
        plane.rows[0].seats[maxSeats-1].window

        and:
        plane.rows[maxRows-1].seats[0].id == ( (maxRows-1)*(maxSeats))
        plane.rows[maxRows-1].seats[0].window

        plane.rows[maxRows-1].seats[maxSeats-1].id == ( (maxRows-1)*(maxSeats) )+maxSeats-1
        plane.rows[maxRows-1].seats[maxSeats-1].window

        where:
        maxRows  |  maxSeats
        4        |  3
        5        |  3
        6        |  2

    }

}
